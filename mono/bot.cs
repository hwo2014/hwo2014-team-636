using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace DOHC
{
    public class Bot
    {
        bool LOG_DATA = false;

        DateTime _startupTime = DateTime.Now;

        public static void Main(string[] args)
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            string file = null;
            if (args.Length > 4 && !args[4].StartsWith("--"))
                file = args[4];

            if (args.Length > 4 && args[4] == "--dummy")
            {
                int random = new Random().Next(0, 9999);
                botName = "Dummy" + random;
            }

            //while (true)
            {
            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            if (file == null)
                StartNetwork(host, port, botName, botKey);
            else
                StartFile(botName, botKey, file);
        }
        }

        // get bot input from a prerecorded file. output doesn't change what's coming in.
        private static void StartFile(string botName, string botKey, string file)
        {
            using (var f = File.OpenRead(file))
            {
                RunBot(f, botName, botKey);
            }
        }


        private static void StartNetwork(string host, int port, string botName, string botKey)
        {
            try
            {
                using (TcpClient client = new TcpClient(host, port))
                {
                    Stream stream = client.GetStream();

                    RunBot(stream, botName, botKey);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("whoa man: {0}", e);
            }
        }

        private static void RunBot(Stream stream, string botName, string botKey)
        {
            StreamReader reader = new StreamReader(stream);
            var outStream = stream;
            if (!(stream is NetworkStream))
                outStream = Console.OpenStandardOutput();
            StreamWriter writer = new StreamWriter(outStream);
            writer.AutoFlush = true;

            // Use these for testing
            //new Bot(reader, writer, new JoinMulti(botName, botKey, "france", 1));
            //new Bot(reader, writer, new JoinMulti(botName, botKey, "usa", 1));
            //new Bot(reader, writer, new JoinMulti(botName, botKey, "germany", 2, "dohc"));
            //new Bot(reader, writer, new JoinMulti(botName, botKey, "usa", 4));
            //new Bot(reader, writer, new JoinMulti(botName, botKey, "france", 1, "dohc"));

            // Needed for CI
            new Bot(reader, writer, new Join(botName, botKey));
        }

        private StreamWriter writer;

        Bot(StreamReader reader, StreamWriter writer, SendMsg join)
        {
            this.writer = writer;
            string line;

            send(join);
            getThrottle = getThrottleProduction;
            while ((line = reader.ReadLine()) != null)
            {
                try
                {
                    HandleLine(line);
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Failed process, line: {0}, ex: {1}", line, e);
                    if (race)
                        send(defaultThrottle);
                    else
                        send(defaultPing);
                }
                
            }
        }

        PathFinder PF { get; set; }

        bool race = true;
        Ping defaultPing = new Ping();
        int? currentTick;

        private void HandleLine(string line)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            currentTick = msg.gameTick;
            Debug.WriteLine(WithState(line));
            switch (msg.msgType)
            {
                case "carPositions":
                    {
                        var throttle = RunAI(msg);
                        lastThrottle = throttle.value;
                        if (msg.gameTick != null)
                            send(throttle);
                    }
                    break;
                case "join":
                    Console.WriteLine("Joined");
                    //noping send(defaultPing);
                    break;
                case "joinRace":
                    var joined = msg.FromJObject<JoinMulti>();
                    Console.WriteLine("Joined. Waiting for {0} opponents...", joined.carCount - 1);
                    
                    //noping send(defaultPing);
                    break;
                case "gameInit":
                    Console.WriteLine("Race init");
                    if (PF == null)
                        PF = new PathFinder(line);
                    if (carTracker == null)
                    {
                        carTracker = new CarTracker(myCar);
                        carTracker.LoadRace(line);
                    }
                    //noping send(defaultPing);
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    race = false;
                    send(defaultPing);
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    race = true;
                    // gamestart is actually first gameticked message.
                    send(new Throttle(1.0));
                    break;
                case "yourCar":
                    SetupCar(msg);
                    Console.WriteLine("My car: {0}", myCar.color);
                    
                    // no ping. send(defaultPing);
                    break;
                case "lapFinished":
                    var lap = (msg.data as JObject).ToObject<LapFinishedData>();
                    Console.WriteLine("{0} laptime for lap #{1}: {2} ms", lap.car.name, lap.lapTime.lap + 1, lap.lapTime.millis);
                    // no ping back!
                    break;

                case "turboAvailable":
                    Console.WriteLine("Turbo available!");
                    Turbo = msg.FromJObject<TurboAvailableData>();
                    // no ping back!
                    break;

                case "turboStart":
                    Console.WriteLine("{0} used turbo!", msg.FromJObject<CarId>().name);
                    break;

                case "turboEnd":
                    Console.WriteLine("{0}'s turbo ended", msg.FromJObject<CarId>().name);
                    break;

                case "dnf":
                    Console.WriteLine("{0} retired", msg.FromJObject<CarId>().name ?? "Someone");
                    break;

                case "finish":
                    Console.WriteLine("{0} finished!", msg.FromJObject<CarId>().name);
                    // no ping back
                    break;
                
                case "crash":
                    var crashInfo = (msg.data as JObject).ToObject<Crash>();

                    if (crashInfo.car.Equals(myCar))
                        Console.WriteLine("FFUUUUU!!!");

                    Console.WriteLine(crashInfo.car.name + " crashed!");
                    break;
                
                case "tournamentEnd":
                    Console.WriteLine("GAME OVER! Thanks for watching");
                    break;
                
                case "spawn":
                    Console.WriteLine("* Respawn *");
                    break;

                default:
                    Console.WriteLine("Unknown message {0}: {1}", msg.msgType, line);
                    send(defaultPing);
                    break;
            }
        }

        private Throttle RunAI(MsgWrapper msg)
        {
            var messageStart = DateTime.Now;

            var carPositions = (msg.data as JArray).ToObject<List<CarPosition>>();
            carTracker.UpdatePositions(carPositions, msg.gameTick);
            var myPos = carPositions.Find(p => p.id.Equals(myCar));

            OptimizeTargetSpeeds(carPositions);
            
            var throttle = getThrottle(myPos, carPositions, msg.gameTick);

            var messageEnd = DateTime.Now;
            if (messageEnd - messageStart > TimeSpan.FromMilliseconds(50))
                Console.WriteLine("WARNING! Execution took more than 50 ms!");

            return throttle;
        }

        private void HandleOpponents(int gameTick, IList<CarPosition> cars, CarPosition myPos, ref double targetSpeed, ref bool overtakingPosition)
        {
            foreach (var car in cars)
            {
                // That's you, stupid
                if (car.id.Equals(myCar))
                    continue;

                var dist = PF.GetDistance(car.piecePosition, myPos.piecePosition, myPos.piecePosition.lane.endLaneIndex);

                // Too far to care about
                if (dist > 100)
                    continue;

                bool inFront = (car.piecePosition.pieceIndex == myPos.piecePosition.pieceIndex // Same piece, but further
                                    && car.piecePosition.inPieceDistance > myPos.piecePosition.inPieceDistance)
                               || car.piecePosition.pieceIndex > myPos.piecePosition.pieceIndex // After current piece
                               || (car.piecePosition.pieceIndex == PF.PiecesCount - 1 // After finish line
                                    && myPos.piecePosition.pieceIndex < PF.PiecesCount / 2);

                // On the same lane
                if (car.piecePosition.lane.startLaneIndex == myPos.piecePosition.lane.endLaneIndex)
                {
                    var speed = carTracker.GetSpeed(car.id);

                    // The bastard is in our way
                    if (inFront && speed > 1) // 0 speed = crashed
                    {
                        if (speed < carTracker.GetSpeed(myCar))
                        {
                            overtakingPosition = true;

                            if (turboEndTick < gameTick)
                            {
                                if (dist > 8)
                                    targetSpeed = speed;
                                else
                                    targetSpeed = speed - 1;
                            }
                        }
                    }
                    /*// Tailgater!
                    else if (!inFront && speed >= carTracker.GetSpeed() - 1 && speed > 4 && dist < 10)
                    {
                        targetSpeed = 0;
                    }*/
                }    
            }
        }

        private void OptimizeTargetSpeeds(IList<CarPosition> cars)
        {
            if (myCar.name.StartsWith("Dummy"))
                return;

            // Init tracking variables
            if (lastPieceId == null)
            {
                lastPieceId = new int[cars.Count];
                lastPieceMaxSpeed = new double[cars.Count];
                lastPieceEntrySpeed = new double[cars.Count];
                lastLane = new int[cars.Count];
                lastMaxAngle = new double[cars.Count];
            }

            // Optimize target speeds
            int index = 0;
            foreach (var car in cars)
            {
                if (car.piecePosition.pieceIndex != lastPieceId[index])
                {
                    PF.UpdateTargetSpeed(lastPieceId[index], lastLane[index], lastPieceMaxSpeed[index], lastMaxAngle[index], lastPieceEntrySpeed[index]);
                    lastPieceMaxSpeed[index] = 0;
                    lastMaxAngle[index] = 0;
                    lastPieceEntrySpeed[index] = carTracker.GetSpeed(car.id);
                }

                lastPieceId[index] = car.piecePosition.pieceIndex;
                lastLane[index] = car.piecePosition.lane.startLaneIndex;
                lastMaxAngle[index] = Math.Max(lastMaxAngle[index], Math.Abs(car.angle));

                var speed = carTracker.GetSpeed(car.id);

                // Most likely crashed
                if (race && speed == 0)
                {
                    PF.UpdateTargetSpeed(lastPieceId[index] + 1, lastLane[index], lastPieceMaxSpeed[index], lastMaxAngle[index], lastPieceEntrySpeed[index], true);
                    lastPieceMaxSpeed[index] = 0;
                }
                // Don't push the envelope too much
                else if (car.id == myCar)
                {
                    if (!espMaxedOut)
                    lastPieceMaxSpeed[index] = Math.Max(speed, lastPieceMaxSpeed[index]);
                }
                // Learn from others
                else
                {
                    lastPieceMaxSpeed[index] = Math.Max(speed, lastPieceMaxSpeed[index]);
                }

                index++;
            }
            
        }

        private string WithState(string line)
        {
            var rp = " ";
            var speed = carTracker != null ? carTracker.GetSpeed().ToString("0.000 m/tick"): "(stopped)";
            if (carTracker != null && carTracker.Me != null && carTracker.Me._lastPosition != null)
            {
                var piece = PF.GetPiece(carTracker.Me._lastPosition);
                if (piece.piece.IsStraight)
                    rp = "|";
                else if (piece.piece.angle < 0)
                    rp = ")";
                else rp = "(";
                rp = string.Format("[{0:00}] {1}", carTracker.Me._lastPosition.pieceIndex, rp);
            }
                

            return string.Format("{4} {0} th: {2:0.0000} {3} {1}", speed, line, lastThrottle, quickStatus, rp);
        }

        private CarId myCar;
        private CarTracker carTracker;

        private void SetupCar(MsgWrapper msg)
        {
            var carid = (msg.data as JObject).ToObject<CarId>();
            myCar = carid;            
        }

        Throttle defaultThrottle = new Throttle(0.57);

        TurboAvailableData Turbo;
        bool useTurboNow = false;
        int turboEndTick;

        double lastThrottle = 0.5;
        double lastAngle = 0;
        DateTime prevDebugPrint = DateTime.Now;
        double lastSpeed = 0;
        int pendingTargetLane = 0;
        
        int[] lastPieceId;
        int[] lastLane;
        double[] lastPieceEntrySpeed;
        double[] lastPieceMaxSpeed;
        double[] lastMaxAngle;
        bool espMaxedOut;
        bool espSlowedDown;

        double deceleration = 0.011;
        double brakingStartSpeed = 0;
        int brakingStartTick = 0;

        Dictionary<int, double> decelerationMap = new Dictionary<int,double>();
         
        Func<CarPosition, IList<CarPosition>, int?, Throttle> getThrottle;
        bool _AccelDecelCalibration = true;
        string quickStatus;

        private Throttle getThrottleProduction(CarPosition myPos, IList<CarPosition> carPositions, int? gameTick)
        {         
            quickStatus = " ";
            if (myCar.name.StartsWith("Dummy"))
            {
                var seed = int.Parse(myCar.name.Replace("Dummy", string.Empty));
                var rnd = new Random(seed);
                return new Throttle(rnd.NextDouble());
            }

            if (myPos == null)
                return defaultThrottle;

            bool printDebug = DateTime.Now - prevDebugPrint > TimeSpan.FromSeconds(1);

            // Route
            SwitchToFastestLane(myPos, carPositions, ref pendingTargetLane);

            if (_AccelDecelCalibration)
                return BrakingCalibration();

            var targetLane = myPos.piecePosition.lane.endLaneIndex;

            if (targetLane != pendingTargetLane)
                targetLane = pendingTargetLane;

            // Speed
            double currentSpeed = carTracker.GetSpeed();
            double targetSpeed = PF.GetCurrentSpeedLimit(myPos);

            // Get speed limit
            double nextSpeedLimit = 0;
            double distanceToSpeedLimit = 0;
            var ticksToBrake = PF.GetTicksToDecelerate(currentSpeed, 0);
            double stoppingDistance = ticksToBrake * currentSpeed;// (Math.Pow(currentSpeed, 2)) / (2 * deceleration);

            PF.GetApproachingSpeedLimit(myPos, currentSpeed, stoppingDistance, targetLane, carPositions,
                    carTracker, out nextSpeedLimit, out distanceToSpeedLimit);
            
            ticksToBrake = PF.GetTicksToDecelerate(currentSpeed,  nextSpeedLimit);
            if (currentSpeed > 0)
            {
                var ticksToNextSpeedLimit = distanceToSpeedLimit / currentSpeed;// MPF.GetTicksToNextSpeedLimit(myPos, currentSpeed, out nextSpeedLimit);

                if (ticksToBrake > 0)
                {
                    if (ticksToNextSpeedLimit - ticksToBrake < 2)
                    {
                        quickStatus = "|";
                        Console.WriteLine("Hit the brakes! ttnsp {0:0.00} ttb {1} nextsp {2:0.00} dtsp {3:0.0} csp {4:0.000} ", 
                            ticksToNextSpeedLimit, ticksToBrake, nextSpeedLimit, distanceToSpeedLimit, currentSpeed);
                        return new Throttle(0.0);
                    }
                }
            }
            
            // Opponents
            bool overtakingPosition = false;
            HandleOpponents(gameTick ?? 0, carPositions, myPos, ref targetSpeed, ref overtakingPosition);

            // Throttle control
            double throttle = lastThrottle;

            if (currentSpeed < targetSpeed * 0.8)
            {
                if (targetSpeed == 99.9 || targetSpeed > currentSpeed * 1.5)
                    throttle = 1; // floor it!
                else
                    throttle += 0.25; // accelerate
            }

            // ESP
            var angled = Math.Abs(myPos.angle) - Math.Abs(lastAngle);
            var angleabs = Math.Abs(myPos.angle);
            espSlowedDown = false;
            espMaxedOut = false;
            throttle = 1;

            if (angleabs > 3)
            {
                // Still cool
                if (angled < -3 && angleabs < 20)
                    throttle += 0.1;
                // Getting risky
                else if (angled < 1 && angleabs < 20)
                    throttle += 0.05;

                // Stop drifting while we can
                if (angled > 2 && angleabs > 10)
                {
                    throttle -= 0.5;
                    espSlowedDown = true;
                    quickStatus = "s";
                }
                
                // Last resort!
                if (angled > 2 && angleabs > 15 && currentSpeed > 5)
                {
                    throttle = 0;
                    espSlowedDown = true;
                    espMaxedOut = true;
                    quickStatus = "E";
                }
                if (angleabs > 45 && currentSpeed > 4)
                {
                    throttle = 0;
                    espMaxedOut = true;
                    espSlowedDown = true;
                    quickStatus = "E";
                }
            }

            // Clamp throttle + speed
            if (throttle > 1)
                throttle = 1;

            if (throttle < 0)
                throttle = 0;

            if (currentSpeed > 20)
                currentSpeed = 20;

            if (currentSpeed < 0)
                currentSpeed = 0;

            // Turbo control 
            if (Turbo != null)
            {
                if (overtakingPosition)
                {
                    useTurboNow = true; // Try to overtake using turbo
                }
                else
                {
                    if (currentSpeed < 1)
                        useTurboNow = true; // Respawned
                    else if (nextSpeedLimit == 99.9 && distanceToSpeedLimit < 5)
                        useTurboNow = true; // Fast acceleration to max
                }
            }

            // Save values
            lastAngle = myPos.angle;
            lastThrottle = throttle;
            lastSpeed = currentSpeed;

            if (printDebug && !(lastSpeed == 0 && currentSpeed == 0))
            {
                Console.WriteLine(
                    "[" + myPos.piecePosition.pieceIndex + "," + myPos.piecePosition.lane.endLaneIndex + "]"
                    + " Throttle: " + Math.Round(throttle, 2)
                    + " Speed: " + Math.Round(currentSpeed, 2) 
                    + " / " + (targetSpeed == 99.9 ? "-" : Math.Round(targetSpeed,2).ToString())
                    +  (espSlowedDown ? " !ESP!" : "")
                    //+ " Deceleration: " + Math.Round(deceleration, 3)
                    );
                prevDebugPrint = DateTime.Now;
            }

            if (LOG_DATA)
            {
                using (var writer = System.IO.File.AppendText(_startupTime.ToFileTimeUtc() + ".csv"))
                {
                    writer.WriteLine(currentSpeed + ";" + targetSpeed + ";" + throttle + ";" + deceleration);
                }
            }

            // nobody sent anything, so turbo can be used
            if (lastSentTick != currentTick && useTurboNow && Turbo != null && currentSpeed > 0 && (angleabs < 10 && angled < 2))
            {
                turboEndTick = (gameTick ?? 0) + Turbo.turboDurationTicks;

                Console.WriteLine("*** TURBO BOOST! ***");
                Debug.WriteLine("Turbo");
                Turbo = null;
                useTurboNow = false;
                send(new TurboMsg());                
            }
            // send switch
            else if (lastSentTick != currentTick && myPos.piecePosition.lane.endLaneIndex != targetLane && laneSwitchCurrent )
            {
                int direction = 0;

                if (targetLane < myPos.piecePosition.lane.endLaneIndex)
                    direction = -1;
                else if (targetLane > myPos.piecePosition.lane.endLaneIndex)
                    direction = 1;

                send(new SwitchLane(direction));
                laneSwitchCurrent = false;                
            }

            return new Throttle(throttle);
            
        }
        
        List<double> _brakingSpeeds = new List<double>();
        List<double> _accelSpeeds = new List<double>();        
        int ticks = 0;

        private Throttle BrakingCalibration()
        {            
            ticks++;
            if (ticks < 20)
            {
                // acceleration phase.
                _accelSpeeds.Add(carTracker.Me.speed);
                return new Throttle(1.0);
            }
            else if (ticks < 40)
            {
                // deceleration phase
                _brakingSpeeds.Add(carTracker.Me.speed);
                return new Throttle(0.0);
            }
            else
            {
                // update and terminate
                PF.SetupAcceleration(1.0, _accelSpeeds);
                PF.SetupDeceleration(0.0, _brakingSpeeds);
                _AccelDecelCalibration = false;
                Debug.WriteLine("Brake & acceleration calibration done.");
                Debug.WriteLine("Accel: " + PF.accelparams);
                Debug.WriteLine("Decel: " + PF.decelparams);
            }

            return new Throttle(1.0);            
        }

        int pieceSwitchSent = 0;
        private Throttle getThrottleSwitcher(CarPosition pos, IList<CarPosition> carPositions, int? gameTick)
        {
            if (
                pieceSwitchSent != pos.piecePosition.pieceIndex
                && PF.NextPieceSwitch(pos.piecePosition)                
                ) 
            {
                var next = pos.piecePosition.lane.endLaneIndex == 0 ? 1: -1;
                send(new SwitchLane(next));
                pieceSwitchSent = pos.piecePosition.pieceIndex;
            }
            return new Throttle(0.05);
        }
        int braketest = 3;
        private Throttle getThrottleOptimalSwitcher(CarPosition pos, IList<CarPosition> carPositions, int? gameTick)
        {
            int temp = 0;
            SwitchToFastestLane(pos, carPositions, ref temp);

            switch (braketest)
            {
                case 0:
                    if (pos.piecePosition.pieceIndex < 39) break;
                    braketest = 1;
                    return new Throttle(0);
                case 1:
                    if (carTracker.GetSpeed() < 0.02)
                    {
                        braketest = 2;
                        break;
                    }
                    return new Throttle(0);
                default:
                    break;
            }

            // suora tulossa? kaasua!
            if (PF.NextPieceStraight(pos.piecePosition.pieceIndex) )
                return new Throttle(1.0);

            //tulossa mutka
            if (carTracker.GetSpeed() > 7)
                return new Throttle(0.1);
            var angled = pos.angle - lastAngle;
            var angleabs = Math.Abs(pos.angle);
            lastAngle = pos.angle;
            

            // reasonable angle yet
            if (angled < -3 && angleabs < 20)
                return new Throttle(clampThrottle(lastThrottle * 1.1));
            if (angled < 1 && angleabs < 20)
                return new Throttle(clampThrottle(lastThrottle * 1.05));
            if (angled > 2 && angleabs > 15)
                return new Throttle(0.05);
            if (angled > 2 && angleabs > 10)
                return new Throttle(clampThrottle(lastThrottle * 0.90));
            
            
            // empirical for keimola that it stays on track.
            return new Throttle(0.57);            
        }
        bool laneSwitchCurrent = false;
        private void SwitchToFastestLane(CarPosition myPos, IEnumerable<CarPosition> carPositions, ref int laneToSwitchTo)
        {
            if (
                pieceSwitchSent != myPos.piecePosition.pieceIndex
                && PF.NextPieceSwitch(myPos.piecePosition)
                )
            {
                int direction;
                var nlane = PF.GetFastestLane(myPos.piecePosition.pieceIndex + 1,
                      myPos.piecePosition.lane.endLaneIndex, myPos, carPositions, carTracker, out direction);

                if (direction != 0)
                {
                    Console.WriteLine("[" + myPos.piecePosition.pieceIndex + "," + myPos.piecePosition.lane.startLaneIndex + "] Switching lane to {0}", nlane);
                    //send(new SwitchLane(direction));
                    laneToSwitchTo = nlane;
                    laneSwitchCurrent = true;
                }

                // even if we didn't really.
                pieceSwitchSent = myPos.piecePosition.pieceIndex;
            }
        }

        private double clampThrottle(double throttle)
        {
            return Math.Max(Math.Min(throttle, 1.0), 0.0);
        }
        int? lastSentTick;
        private void send(SendMsg msg)
        {
            if (msg.MType == "ping")
                Debug.WriteLine("pingd");
            
            string r = msg.ToJson();
            
            if (currentTick != null && lastSentTick == currentTick)
                Console.WriteLine("WARNING: double message detected.");

            lastSentTick = currentTick;
            //Console.WriteLine(r);
            writer.WriteLine(r);
        }
    }
}