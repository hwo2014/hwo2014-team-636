﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DOHC
{
    public class CarTracker
    {
        public class CarPositionData
        {
            public PiecePosition _lastPosition;
            int _lastTick;
            public double previousSpeed;
            public double speed;
            
            internal void Update(CarPosition p, int? tick)
            {
                var dTick = (tick ?? 0) - _lastTick;

                if (dTick == 0)
                    return;

                if (_lastPosition == null)
                    _lastPosition = p.piecePosition;

                var distance =
                    PathFinder.Instance.GetDistance(_lastPosition, p.piecePosition, 
                                p.piecePosition.lane.startLaneIndex);

                previousSpeed = speed;
                if (dTick != 0)
                    speed = distance / dTick;

                // workaround wrong speed when in switch and piece changed
                if (_lastPosition.lane.endLaneIndex != p.piecePosition.lane.startLaneIndex
                    && _lastPosition.pieceIndex != p.piecePosition.pieceIndex)
                    speed = previousSpeed;

                _lastTick = tick ?? 0;
                _lastPosition = p.piecePosition;
            }

            internal bool TerminalSpeed()
            {
                return Math.Abs(previousSpeed - speed) < 0.005;
            }
        }
        DimensionedCar[] _cars;
        public CarPositionData Me { get; set; }
        private CarId carId;
        Dictionary<CarId, CarPositionData> _carPositions;

        public CarTracker(CarId carId)
        {
            // TODO: Complete member initialization
            this.carId = carId;
        }

        public void LoadRace(string gameInit)
        {
            var msg = JsonConvert.DeserializeObject<MsgWrapper>(gameInit);
            var race = (msg.data as Newtonsoft.Json.Linq.JObject)["race"].ToObject<Race>();
            _cars = race.cars;
            _carPositions = race.cars.ToDictionary(c => c.id, c => new CarPositionData { });
            Me = _carPositions[carId];
        }

        public void UpdatePosition(string carPositions)
        {
            var msg = JsonConvert.DeserializeObject<MsgWrapper>(carPositions);
            var positions = (msg.data as Newtonsoft.Json.Linq.JArray).ToObject<List<CarPosition>>();
         
            UpdatePositions(positions, msg.gameTick);
        }

        public void UpdatePositions(List<CarPosition> positions, int? tick)
        {
            foreach (var p in positions)
                _carPositions[p.id].Update(p, tick);
        }        

        public double GetSpeed(CarId id = null)
        {
            if (id == null)
                return Me.speed;

            return _carPositions[id].speed;
        }
    }
}
