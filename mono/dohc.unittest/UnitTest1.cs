﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DOHC;
using System.Linq;
using MathNet.Numerics;
using System.Diagnostics;

namespace dohc.unittest
{
    [TestClass]
    public class PathHelpersTests
    {
        [TestMethod]
        public void TrackPiece_SwitchLengths()
        {
            var tracklanes = GetLanes4();            
            var straightPiece = new TrackPiece { length = 100, @switch = true };
            var straightlengths = new[] { 100.0, 100.0, 100.0, 100.0 };
            var leftlane = tracklanes[0];
            var empty = new double[4];

            var lengths = straightPiece.CalculateSwitchLengths(leftlane, straightlengths, tracklanes, empty).ToArray();
            var expected_distance = Math.Sqrt(100 * 100 + 20 * 20);

            Assert.AreEqual(1, lengths.Length, "only one switch for left");
            Assert.AreEqual(expected_distance, lengths[0], "hypotenusa doesn't match");
            
            var rightlane = tracklanes[3];

            lengths = straightPiece.CalculateSwitchLengths(rightlane, straightlengths, tracklanes, empty).ToArray();

            Assert.AreEqual(1, lengths.Length, "only one switch for right");
            Assert.AreEqual(expected_distance, lengths[0], "hypotenusa doesn't match");

            var middlelane = tracklanes[1];

            lengths = straightPiece.CalculateSwitchLengths(middlelane, straightlengths, tracklanes, empty).ToArray();

            Assert.AreEqual(2, lengths.Length, "can switch to both directions");
            Assert.AreEqual(expected_distance, lengths[0], "hypotenusa doesn't match");
            Assert.AreEqual(expected_distance, lengths[1], "hypotenusa doesn't match");

            middlelane = tracklanes[2];

            lengths = straightPiece.CalculateSwitchLengths(middlelane, straightlengths, tracklanes, empty).ToArray();

            Assert.AreEqual(2, lengths.Length, "can switch to both directions");
            Assert.AreEqual(expected_distance, lengths[0], "hypotenusa doesn't match");
            Assert.AreEqual(expected_distance, lengths[1], "hypotenusa doesn't match");
        }

        private static TrackLane[] GetLanes4()
        {
            var tracklanes = new[] {
                new TrackLane { index = 0, distanceFromCenter = -30},
                new TrackLane { index = 1, distanceFromCenter = -10},
                new TrackLane { index = 2, distanceFromCenter = 10},
                new TrackLane { index = 3, distanceFromCenter = 30}
            };
            return tracklanes;
        }

        [TestMethod]
        public void TrackPiece_CalcSwitchLengthsTurn()
        {
            var lanes = GetLanes4();
            // angle means turning to right, so left length is always larger
            var turnpiece = new TrackPiece { angle = 180, radius = 100, @switch = true };
            var lengths = lanes.Select(l => turnpiece.CalculateLength(l)).ToArray();
            var empty = new double[4];

            var leftlane = lanes[0];

            var swlengths = turnpiece.CalculateSwitchLengths(leftlane, lengths, lanes, empty).ToArray();

            Assert.AreEqual(1, swlengths.Length, "only one switch");
            Assert.IsTrue(swlengths[0] < lengths[0], "switch on right turn is shorter than left lane");
            Assert.IsTrue(swlengths[0] > lengths[1], "switch 0->1 is longer than lane 1");
                
            var rightlane = lanes[3];
            swlengths = turnpiece.CalculateSwitchLengths(rightlane, lengths, lanes, empty).ToArray();

            Assert.AreEqual(1, swlengths.Length, "only one switch");
            Assert.IsTrue(swlengths[0] > lengths[3], "switch on right turn is longer than right lane");
            Assert.IsTrue(swlengths[0] < lengths[2], "switch 2->3 is longer than lane 2");

            swlengths = turnpiece.CalculateSwitchLengths(lanes[1], lengths, lanes, empty).ToArray();
            Assert.AreEqual(2, swlengths.Length, "two switches");
            Assert.IsFalse(swlengths[0] == swlengths[1], "shouldn't be the same");

            swlengths = turnpiece.CalculateSwitchLengths(lanes[2], lengths, lanes, empty).ToArray();
            Assert.AreEqual(2, swlengths.Length, "two switches");
            Assert.IsFalse(swlengths[0] == swlengths[1], "shouldn't be the same");
        }

        [TestMethod]
        public void FittingTests()
        {
            var acc = new double[]{   0, 0.19, 0.37, 0.55, 0.73, 0.9, 1.07, 1.24, 1.4};
            var ticks = new double[]{ 0, 1,    2,    3,    4,    5,   6,    7,    8};
            var t = Fit.Polynomial(acc, ticks, 2);
            foreach (var x in t)
                Debug.WriteLine(x);
            
        }
    }
}
