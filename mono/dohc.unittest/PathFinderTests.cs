﻿using DOHC;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace dohc.unittest
{
    [TestClass]
    public class PathFinderTests
    {
        [TestMethod]
        public void BuildKeimola()
        {
            var p = GetFinderWithKeimola();

            Assert.AreEqual(40, p.PiecesCount, "keimola size not there");
            Assert.AreEqual(2, p.LanesCount, "our keimola has two tracks");
        }

        [TestMethod]
        public void NextPieceBasic()
        {
            var p = GetFinderWithKeimola();

            var nextPieceStraight = p.NextPieceStraight(0);

            Assert.IsTrue(nextPieceStraight, "keimola second piece is straight");

            nextPieceStraight = p.NextPieceStraight(39);

            Assert.IsTrue(nextPieceStraight, "keimola first piece is straight");

            nextPieceStraight = p.NextPieceStraight(3);

            Assert.IsFalse(nextPieceStraight, "keimola first turn is 4th piece");
        }

        [TestMethod]
        public void Distance()
        {
            var pf = GetFinderWithKeimola();

            var p1 = new PiecePosition { pieceIndex = 0, inPieceDistance = 10 };
            var p2 = new PiecePosition { pieceIndex = 0, inPieceDistance = 20 };           

            var d = pf.GetDistance(p1, p2, 0);
            Assert.AreEqual(10, d, "well it should be 10");

            var p3 = new PiecePosition { pieceIndex = 4, inPieceDistance = 30 };
            d = pf.GetDistance(p1, p3, 0);

            Assert.AreEqual((100 - 10) + 3 * 100 + 30, d, "first non-straight");
        }

        [TestMethod]
        public void PieceLength()
        {
            var pf = GetFinderWithKeimola();

            var p1 = new PiecePosition { pieceIndex = 7, inPieceDistance = 0 };
            var p2 = new PiecePosition { pieceIndex = 8, inPieceDistance = 0 };

            var d = pf.GetLaneLength(8, 0);
            var d2 = pf.GetLaneLength(8, 1);

            Assert.IsTrue(CarTrackerTests.WithinD(d, 82.4, 0.1));
            Assert.IsTrue(CarTrackerTests.WithinD(d2, 74.6, 0.1));
            

            //d = pf.GetLaneLength()
            
        }

        /*[TestMethod]
        public void FastestLaneDistance()
        {
            var pf = GetFinderWithKeimola();

            int directchange;
            var sdist = pf.GetFastestLane(39, 0, out directchange);

            Assert.AreEqual(0, directchange, "no change at the end");

            sdist = pf.GetFastestLane(8, 1, out directchange);
            Assert.AreEqual(0, sdist, "lane 8 is best here");
        }*/

        [TestMethod]
        public void AccelerationCalc()
        {
            var expA = 0.0809;
            var expB = -0.022;
            var speeds = new List<double>() {0, 0, 0.28222243, 0.357983503, 0.432094032, 0.504589976, 0.57550651, 0.644878044, 0.712738238, 0.779120017, };

            var pf = GetFinderWithKeimola();
            pf.SetupAcceleration(1.0, speeds);

            Assert.IsTrue(Within(expA, pf.accelparams.a, 0.0001));
            Assert.IsTrue(Within(expB, pf.accelparams.b, 0.0001));
            Assert.AreEqual(speeds[3], pf.accelparams.vStart);
            Assert.IsTrue(Within(speeds[4], pf.accelparams.GetSpeedStep(4), 0.0001));
        }

        [TestMethod]
        public void DecelCalc()
        {
            var expA = 4.221689;
            var expB = -0.01959;
            var speeds = new List<double>() { 4.055200904, 4.059454625, 3.980690367, 3.903454345, 3.827716909, 3.753448981, 3.68062205, 3.609208155, 3.539179881, 3.470510342, 3.403173175, };

            var pf = GetFinderWithKeimola();
            pf.SetupDeceleration(0.0, speeds);
            Assert.IsTrue(Within(expA, pf.decelparams.a, 0.0001));
            Assert.IsTrue(Within(expB, pf.decelparams.b, 0.0001));

            var idx = pf.decelparams.GetIndex(40.0);
            Assert.AreEqual(0, idx);

            idx = pf.decelparams.GetIndex(0.0001);
            Assert.AreEqual(pf.decelparams.decel_tbl.Length - 1, idx);

            var ttd = pf.GetTicksToDecelerate(speeds[1], speeds[8]);
            Assert.AreEqual(6, ttd);

            ttd = pf.GetTicksToDecelerate(speeds[2], speeds[8]);
            Assert.AreEqual(5, ttd);
        }

        Func<double, double, double, bool> Within = CarTrackerTests.WithinD;

        private static PathFinder GetFinderWithKeimola()
        {
            var p = new PathFinder(File.ReadAllText("keimola.json"));
            
            return p;
        }
    }
}
