﻿using DOHC;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace dohc.unittest
{
    [TestClass]
    public class CarTrackerTests
    {
        [TestMethod]
        public void BasicTracker()
        {
            var ct = new CarTracker(new CarId { name = "DOHC", color = "red" });
            ct.LoadRace(File.ReadAllText("keimola.json"));

            var positions = GetPositionDataKeimolaThrottle7().Skip(10).Take(2).ToArray();
            ct.UpdatePosition(positions[0]);
            ct.UpdatePosition(positions[1]);

            var speed = ct.GetSpeed();
            var wantspeed = 5.975482410641412-4.811716745552461; // /1 tick
            var deltaSmall = WithinD(speed, wantspeed, 0.001);
            Assert.IsTrue(deltaSmall, "delta between values should be small");
        }

        [TestMethod]
        public void CarId()
        {
            var carA = new CarId { color = "blue", name = "a" };
            var carB = new CarId { color = "white", name = "a" };
            var carC = new CarId { color = "blue", name = "a" };

            Assert.IsTrue(carA == carC);
            Assert.IsFalse(carA == carB);

            Assert.IsTrue(carA != carB);
            Assert.IsFalse(carA != carC);
        }
        

        public static bool WithinD(double a, double b, double delta)
        {
            return Math.Abs(a - b) < delta;
        } 

        private IEnumerable<string> GetPositionDataKeimolaThrottle7()
        {
            return File.ReadLines("throttle-7.dat").Where(s => s.Contains("carPositions")).ToList();
        }
    }
}
