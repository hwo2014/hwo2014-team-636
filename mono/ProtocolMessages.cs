﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace DOHC
{
    class MsgWrapper
    {
        public string msgType;
        public Object data;
        public string gameId;
        public int? gameTick;

        public T FromJObject<T>()
        {
            return (data as JObject).ToObject<T>();
        }
        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }

    abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
        public string MType { get { return MsgType(); } }
    }

    class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
    class JoinMulti : SendMsg
    {
        public class BotId
        {
            public string name;
            public string key;
        }
        public BotId botId;
        public string trackName;
        public int carCount;
        public string password;
        public JoinMulti(string name, string key, string track, int cars, string pass = null)
        {
            botId = new BotId { name = name, key = key };
            trackName = track;
            carCount = cars;
            password = pass;
        }
        protected override string MsgType()
        {
            return "joinRace";
        }
    }

    class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

    class Crash : SendMsg
    {
        public CarId car;

        public Crash(string name, string color)
        {
            car = new CarId()
                    {
                        name = name,
                        color = color
                    };
        }

        protected override string MsgType()
        {
            return "crash";
        }
    }

    class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }

    class SwitchLane : SendMsg 
    {
        public int switch_direction;
        public SwitchLane(int direction)
        {
            switch_direction = direction;
        }
        protected override object MsgData()
        {
            return switch_direction == -1? "Left": "Right";
        }
        protected override string MsgType()
        {
            return "switchLane";
        }
    }

    class TurboMsg : SendMsg
    {
        protected override object MsgData()
        {
            return "Turbo boost!";
        }
        protected override string MsgType()
        {
            return "turbo";
        }
    }
    public class CarId
    {
        public string name;
        public string color;
        public override bool Equals(object obj)
        {            
            if (obj == null)
                return base.Equals(obj);

            var o = obj as CarId;
            return o.name == name
                && o.color == color;
        }
        public override int GetHashCode()
        {
            return (name.GetHashCode() * 23) ^ color.GetHashCode();
        }
        public static bool operator ==(CarId a, CarId b) 
        {
            // can't compare as cars if they're nulls.
            var ao = a as object;
            var bo = b as object;
            if (ao == null || bo == null)
                return ao == bo;
                
            return a.Equals(b);
        }
        public static bool operator != (CarId a, CarId b)
        {
            return !(a == b);
        }
    }
    public class PiecePosition
    {
        public int pieceIndex;
        public double inPieceDistance;
        public LaneInfo lane;        
    }
    public class LaneInfo
    {
        public int startLaneIndex;
        public int endLaneIndex;
    }
    public class CarPosition
    {
        public CarId id;
        public double angle;
        public PiecePosition piecePosition;        
        public int lap;
    }

    public class TrackPiece
    {
        public double length;
        public bool @switch;
        public double angle;
        public double radius;

        public bool IsStraight { get { return length > 0.0; } }

        public double CalculateLength(TrackLane lane)
        {
            if (length > 0)
                return length;
            
            double r = radius;

            if (angle <= 0)
                r += lane.distanceFromCenter;
            else
                r -= lane.distanceFromCenter;

            return Math.Abs(angle / 180.0 * Math.PI * r);
        }        

        public IEnumerable<double> CalculateSwitchLengths(TrackLane lane, double[] lanelengths, TrackLane[] lanes, double[] extras)
        {
            var index = lane.index;
            if (index >= 0 && index < (lanelengths.Length-1)) // can switch up
            {
                var uplane = lanes[index+1];
                yield return CalcSwitchLength(lanelengths[index], lanelengths[index+1], lane, uplane) + extras[index+1];
            }
            if (index > 0 && index < lanelengths.Length ) // only can switch down.
            {
                var downlane = lanes[index - 1];
                yield return CalcSwitchLength(lanelengths[index], lanelengths[index-1], lane, downlane) + extras[index -1];
            } 
        }

        private double CalcSwitchLength(double lenA, double lenB, TrackLane source, TrackLane dest)
        {
            if (IsStraight)
            {
                var d = source.distanceFromCenter - dest.distanceFromCenter;
                return Math.Sqrt(lenA * lenA + d*d);
            }
            var dlanes = Math.Abs(lenA - lenB);
            // fixme: this is just an approximation as we don't know how turning switch lane is calculated.
            return Math.Abs(Math.Min(lenA, lenB) + dlanes * 0.7);

        }
    }
    public class TrackLane
    {
        public double distanceFromCenter;
        public int index;
    }
    class xypos {
        public double x;
        public double y;
    }
    class StartPosition
    {
        public xypos position;
        public double angle;
    }
    class Track
    {
        public string id;
        public string name;
        public TrackPiece[] pieces;
        public TrackLane[] lanes;
        public StartPosition startingPoint;
    }
    public class CarDimension {
        public double length;
        public double width;
        public double guideFlagPosition;
    }
    public class DimensionedCar {
        public CarId id;
        public CarDimension dimensions;
    }
    public class RaceSession
    {
        public int laps;
        public int maxLapTimeMs;
        public bool quickRace;
    }
    class Race
    {
        public Track track;
        public DimensionedCar[] cars;
        public RaceSession raceSession;
    }

    class LapFinishedData
    {
        public CarId car;
        public LapTime lapTime;
        public RaceTime raceTime;
        public Ranking ranking;

        public class Ranking
        {
            public int overall;
            public int fastestLap;
        }

        public class RaceTime
        {
            public int laps;
            public int ticks;
            public int millis;
        }

        public class LapTime
        {
            public int lap;
            public int ticks;
            public int millis;
        }
    }
    public class TurboAvailableData
    {
        public double turboDurationMilliseconds;
        public int turboDurationTicks;
        public double turboFactor;
    }
}
