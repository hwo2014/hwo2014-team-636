﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DOHC
{
    public class PathFinder
    {
        private double _friction = 0.3;

        public class PiecePathData
        {
            public TrackPiece piece;
            // distance from start by lane _switches not accounted_ this is for speedometer            
            public double[] distanceFromStart;

            // throttle level that should be safe using the lane
            public double[] maxSpeed;

            // shortest distance to end from beginning of this lane
            public double[] distanceToEnd;

            // length of each lane
            public double[] laneLength;

            public double[] nonCrashSpeed;

            public TrackLane[] lanes;

            internal void InitPrevious(PiecePathData prev, TrackLane[] lanes, double friction)
            {               
                distanceFromStart = new double[lanes.Length];
                maxSpeed = new double[lanes.Length];
                laneLength = new double[lanes.Length];
                nonCrashSpeed = new double[lanes.Length];
                this.lanes = lanes;
                                
                int i = 0;
                foreach (var lane in lanes)
                {
                    if (prev != null)
                    {
                        distanceFromStart[i] = prev.distanceFromStart[i]
                            + prev.piece.CalculateLength(lane);
                    }

                    laneLength[i] = piece.CalculateLength(lane);
                    maxSpeed[i] = GuessMaxSpeed(piece, lane, friction);

                    i++;
                }                
            }

            public double GuessMaxSpeed(int lane, double friction)
            {
                return GuessMaxSpeed(this.piece, this.lanes[lane], friction);
            }

            private double GuessMaxSpeed(TrackPiece piece, TrackLane lane, double friction)
            {
                /* Test different values to get these right */
                double f_max = friction; // Force required to overcome friction
                const double m = 1; // Car mass

                if (piece.IsStraight)
                    return 99.9;

                double r = piece.radius;

                if (piece.angle <= 0)
                    r += lane.distanceFromCenter;
                else
                    r -= lane.distanceFromCenter;

                // Centripetal force
                // F = (m(v^2))/r

                double v_max = Math.Sqrt((f_max * r) / m);

                return v_max;
            }
            public static void UpdateEndDistances(PiecePathData[] pieces, TrackLane[] lanes)
            {
                var cnt = pieces.Length;
                PiecePathData next = null;
                int i;
                while(cnt-- > 0)
                {
                    var piece = pieces[cnt];
                    var dToEnd = piece.distanceToEnd = new double[lanes.Length];
                    i = 0;
                    foreach(var lane in lanes) {
                        dToEnd[i] = CalcShortestLength(piece, lane, lanes, next);
                        //if (next != null) {
                        //    dToEnd[i] += next.distanceToEnd[i];
                        //}
                        i++;
                    }
                    next = piece;                    
                }
            }

            private static double CalcShortestLength(PiecePathData piece, TrackLane lane, TrackLane[] lanes, PiecePathData next)
            {
                double[] addata;
                if (next != null)
                    addata = next.distanceToEnd;
                else addata = new double[lanes.Length];
                var ownlane = piece.laneLength[lane.index] + addata[lane.index];
                if (piece.piece.@switch)
                {                    
                    double? min = piece.piece.CalculateSwitchLengths(lane, piece.laneLength, lanes, addata)
                                    .Min();
                    if (ownlane > (min ?? double.MaxValue))
                        return min.Value;                    
                }
                return ownlane;
            }
        }
        // Racetrack
        private Track _track;
        PiecePathData[] _pieceData;

        static PathFinder _instance;
        public static PathFinder Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new PathFinder();
                return _instance;
            }
        }
        private PathFinder()
        {
        }

        public PathFinder(string gameInit )
        {
            _instance = this;
            BuildFromGameInit(gameInit);
        }

        public static double CalculateFriction(double v_max, double r)
        {
            // F = (m(v^2))/r
            double f_max = Math.Pow(v_max, 2) / r;

            return f_max;
        }

        public void BuildFromGameInit(string gameInit)
        {
            var msg = JsonConvert.DeserializeObject<MsgWrapper>(gameInit);
            var race = (msg.data as Newtonsoft.Json.Linq.JObject)["race"].ToObject<Race>();
            _track = race.track;
            int i = 0;
            PiecePathData prev = null;
            _pieceData = new PiecePathData[PiecesCount];
            foreach (var p in _track.pieces)
            {
                var ppd = new PiecePathData { piece = p };
                ppd.InitPrevious(prev, _track.lanes, _friction);
                prev = ppd;
                _pieceData[i++] = ppd;
            }
            PiecePathData.UpdateEndDistances(_pieceData, _track.lanes);
        }

        public int PiecesCount { get { return _track.pieces.Length; } }

        public int LanesCount { get { return _track.lanes.Length; } }

        public bool NextPieceStraight(int currentPiece)
        {
            currentPiece = (currentPiece + 1) % PiecesCount;
            return _track.pieces[currentPiece].IsStraight;
        }

        public double GetDistanceToNextPiece(PiecePosition currentPosition)
        {
            var nextPiece = (currentPosition.pieceIndex + 1) % PiecesCount;
            var nextPos = new PiecePosition() 
                            { 
                                inPieceDistance = 0,
                                pieceIndex = nextPiece,
                                lane = currentPosition.lane
                            };

            return GetDistance(currentPosition, nextPos, nextPos.lane.startLaneIndex);
        }

        public double GetCurrentSpeedLimit(CarPosition pos)
        {
            int index = pos.piecePosition.pieceIndex;
            int lane = pos.piecePosition.lane.endLaneIndex;

            return _pieceData[index].maxSpeed[lane];
        }

        public void GetApproachingSpeedLimit(CarPosition pos, double mySpeed, double brakingDist, 
            int targetLane, IList<CarPosition> carPositions, CarTracker carTracker, 
            out double targetSpeed, out double distanceToTargetSpeed)
        {
            double minSpeed = 99.9;
            double distToMin = 0;

            double firstSpeed = 99.9;
            double distToFirst = 0;

            double carSpeed = 99.9;
            double distToCar = 9999;

            double dist = 0;
            
            int index = pos.piecePosition.pieceIndex;
            int lane = pos.piecePosition.lane.endLaneIndex;
            int failsafe = 10;

            while (dist <= brakingDist)
            {
                var piece = _pieceData[index];

                if (piece.piece.@switch)
                    lane = targetLane;

                if (dist > 0 && piece.maxSpeed[lane] < minSpeed)
                {
                    minSpeed = piece.maxSpeed[lane];
                    distToMin = dist;
                }

                if (dist > 0 && firstSpeed == 99.9)
                {
                    firstSpeed = piece.maxSpeed[lane];
                    distToFirst = dist;
                }

                dist += piece.laneLength[lane];

                foreach (var carPos in carPositions)
                {
                    if (carPos.piecePosition.pieceIndex != index || carPos.id == pos.id)
                        continue;

                    var speed = carTracker.GetSpeed(carPos.id);

                    if (speed < 1.0)
                        continue; 

                    carSpeed = Math.Min(speed, carSpeed);
                    distToCar = Math.Min(distToCar, dist + carPos.piecePosition.inPieceDistance);
                }

                index = (index + 1) % _pieceData.Length;
                
                failsafe--;
                if (failsafe == 0)
                    break;
            }

            // Adjust the distances as our position on the piece was not included
            distToFirst = Math.Max(0, distToFirst - pos.piecePosition.inPieceDistance);
            distToMin = Math.Max(0, distToMin - pos.piecePosition.inPieceDistance);
            distToCar = Math.Max(0, distToCar - pos.piecePosition.inPieceDistance);

            // Calculate the final target speed
            double d_0 = brakingDist - distToFirst;
            double d_min = brakingDist - distToMin;

            double w = 1;

            // Use the first speed limit
            if (d_0 < 0)
                w = 0;
            // Use the slowest speed limit
            else if (d_min < 0)
                w = 1;
            // Use a linear interpolation of the two 
            else if (d_min != 0 && d_0 < d_min)
                w = (d_0 / d_min);
            else if (d_0 != 0 && d_min < d_0)
                w = 1 - (d_min / d_0);

            var interpolated = firstSpeed * (1 - w) + minSpeed * w;
            
            // Only change the target speed down from the first speed limit
            targetSpeed = Math.Min(interpolated, Math.Min(firstSpeed, carSpeed));
            distanceToTargetSpeed = Math.Min(distToFirst, Math.Min(distToMin, distToCar));
        }

        public void UpdateTargetSpeed(int piece, int lane, double speed, double angleabs, double entryspeed, bool crashed = false)
        {
            var pid = NormalizePid(piece);

            if (_pieceData[pid].piece.IsStraight)
                return;

            var maxSpeed = speed;
            speed = entryspeed;

            if (maxSpeed > entryspeed * 5)
                maxSpeed = entryspeed * 3;

            var originalSpeed = _pieceData[pid].maxSpeed[lane];
            var noncrashSpeed = _pieceData[pid].nonCrashSpeed[lane];            

            // update safe speed when they crash.
            if (!crashed)
                _pieceData[pid].nonCrashSpeed[lane] = Math.Max(noncrashSpeed, entryspeed);

            if (crashed)
            {
                // went in harder than our estimate.
                if (entryspeed > originalSpeed)
                    return;

                speed = noncrashSpeed < 0.5 ? originalSpeed*0.75: noncrashSpeed;

            } else if (angleabs < 5 && maxSpeed > speed) {
                speed = maxSpeed;
            }
            else if(angleabs < 20 && maxSpeed > speed)
            {
                // low enough angle to take it as weighted
                var angleweight = angleabs / 60.0;
                speed = angleweight * speed + (1.0-angleweight)*maxSpeed;
            } 
            else if (angleabs < 50 && maxSpeed > speed) 
            {                
                var dV = maxSpeed - speed;
                speed += dV * 0.8;                
            }            

            if (!crashed && speed < originalSpeed && angleabs < 45)
                return;

            // Let's be careful
            //speed = (speed + originalSpeed) / 2.0;
            
            _pieceData[pid].maxSpeed[lane] = speed;
            Console.WriteLine("[" + pid + "," + lane + "] Speed limit: " + Math.Round(originalSpeed, 2) + " -> " + Math.Round(speed, 2));

            //// Update other lanes
            //for (int i = 0; i < _pieceData[pid].lanes.Length; i++)
            //{
            //    if (i == lane)
            //        continue;

            //    var f_max = CalculateFriction(speed, _pieceData[pid].piece.radius);
            //    var v_max = _pieceData[pid].GuessMaxSpeed(i, f_max);

            //    if (v_max > _pieceData[pid].maxSpeed[i] || crashed)
            //        _pieceData[pid].maxSpeed[i] = v_max;
            //}

            //UpdateSimilarTargetSpeedsAsync(lane, _pieceData[pid].piece.radius, _pieceData[pid].piece.angle, speed);
        }

        public void UpdateFrictionAsync(double friction)
        {
            Console.WriteLine("Friction re-estimate: " + Math.Round(_friction, 3) + " -> " + Math.Round(friction, 3));
            Thread updateThread = new Thread(UpdateFriction);
            updateThread.Start(friction);
        }

        private void UpdateFriction(object parameter)
        {
            var friction = (double)parameter;
            _friction = friction;

            foreach (var p in _pieceData)
            {
                for (int lane = 0; lane < p.lanes.Length; lane++)
                {
                    var maxSpeed = p.GuessMaxSpeed(lane, friction);

                    p.maxSpeed[lane] = (p.maxSpeed[lane] + maxSpeed) / 2;
                }
            }
        }

        private void UpdateSimilarTargetSpeedsAsync(int lane, double radius, double angle, double speed)
        {
            Thread updateThread = new Thread(UpdateSimilarTargetSpeeds);
            updateThread.Start(new { lane, radius, angle, speed });
        }

        private void UpdateSimilarTargetSpeeds(object parameter)
        {
            dynamic parameters = parameter;

            foreach (var p in _pieceData)
            {
                /*if (Math.Abs(p.piece.angle) < Math.Abs(parameters.angle) - 2
                    && Math.Abs(p.piece.angle) > Math.Abs(parameters.angle) + 2)
                    continue;*/

                if (p.piece.radius != parameters.radius)
                    continue;

                if (p.maxSpeed[parameters.lane] > parameters.speed)
                {
                    var par = new
                        {
                            lane = parameters.lane,
                            radius = parameters.radius,
                            angle = parameters.angle,
                            speed = p.maxSpeed[parameters.lane] 
                        };

                    UpdateSimilarTargetSpeeds(par);
                    return;
                }

                p.maxSpeed[parameters.lane] = parameters.speed;
            }
        }

        public double GetDistance(PiecePosition pA, PiecePosition pB, int lane)
        {
            // fastpath
            if (pA.pieceIndex == pB.pieceIndex)
                return Math.Abs(pA.inPieceDistance - pB.inPieceDistance);

            var pAFromStart = GetPieceDistanceFromStart(pA.pieceIndex, lane);
            var pBFromStart = GetPieceDistanceFromStart(pB.pieceIndex, lane);

            //FIXME: for case across finish line            

            return Math.Abs(pBFromStart + pB.inPieceDistance 
                        - pAFromStart - pA.inPieceDistance);
        }

        private double GetPieceDistanceFromStart(int pieceId, int lane)
        {
            pieceId = NormalizePid(pieceId);
            return _pieceData[pieceId].distanceFromStart[lane];
        }

        internal bool NextPieceSwitch(PiecePosition piecePosition)
        {
            var nextPieceId = NormalizePid(piecePosition.pieceIndex + 1);
            return _pieceData[nextPieceId].piece.@switch;
        }

        public double GetLaneLength(int pieceId, int lane)
        {
            pieceId = NormalizePid(pieceId);
            return _pieceData[pieceId].laneLength[lane];
        }

        public int GetFastestLane(int pieceId, int currentLane, CarPosition myPos, IEnumerable<CarPosition> carPositions, CarTracker carTracker, out int directchange)
        {
            var pid = NormalizePid(pieceId);
            directchange = 0;

            // Route alternatives
            double current = GetLaneTime(pid, currentLane, myPos, carPositions, carTracker);
            double left = GetLaneTime(pid, currentLane - 1, myPos, carPositions, carTracker);
            double right = GetLaneTime(pid, currentLane + 1, myPos, carPositions, carTracker);

            // Suggest a route
            if (left < current && left < right)
                directchange = -1;
            else if (right < current && right < left)
                directchange = 1;
            else
                directchange = 0;

            Console.WriteLine(
                "[" + pieceId + "," + currentLane + "]"
                + " Intersection: " 
                + (left != 9999 ? ((int)left).ToString() : "-") + " < "
                + (current != 9999 ? ((int)current).ToString() : "-") + " > "
                + (right != 9999 ? ((int)right).ToString() : "-"));

            return currentLane + directchange;
        }

        private double GetLaneTime(int pieceId, int currentLane, CarPosition myPos, IEnumerable<CarPosition> carPositions, CarTracker carTracker)
        {
            double totalTime = 0;

            // Route not possible
            if (currentLane < 0 || currentLane >= _pieceData[pieceId].laneLength.Length)
                return 9999;

            bool secondSwitch = false;
            bool carBlocking = false;
            bool carSlow = false;

            // Calculate cumulative length up until the next switch or goal 
            for (int i = pieceId; i < _pieceData.Length; i++)
            {
                if (_pieceData[i].piece.@switch)
                {
                    if (secondSwitch)
                        break;

                    secondSwitch = true;
                }

                var pieceTime = _pieceData[i].laneLength[currentLane] / _pieceData[i].maxSpeed[currentLane];

                // Take opponents into account
                foreach (var carPos in carPositions)
                {
                    if (carPos.piecePosition.pieceIndex != i || carPos.piecePosition.lane.startLaneIndex != currentLane)
                        continue;

                    if (carPos.id.Equals(myPos.id))
                        continue;

                    carBlocking = true;

                    if (carTracker.GetSpeed(carPos.id) < _pieceData[i].maxSpeed[currentLane])
                        carSlow = true;
                }

                
                if (carBlocking && carSlow)
                    pieceTime *= 1.25;

                totalTime += pieceTime;
            }

            return totalTime;
        }

        private int NormalizePid(int pieceId)
        {
            return pieceId % PiecesCount;
        }

        public AccelParams accelparams;
        public void SetupAcceleration(double p, List<double> _accelSpeeds, int cnt = 3)
        {            
            var vA = _accelSpeeds[cnt];
            var vB = _accelSpeeds[cnt+1];
            var vC = _accelSpeeds[cnt+2];
            var dv1 = vB - vA;
            var dv2 = vC - vB;
            var b = Math.Log(dv2 / dv1) ;
            var a = dv1 / Math.Exp(b * (cnt+1));

            accelparams = new AccelParams { vStart = vA, a = a, b = b, vIdx = cnt };
        }

        public AccelParams decelparams;
        public void SetupDeceleration(double p, List<double> _decelSpeed, int cnt = 2)
        {
            var vA = _decelSpeed[cnt];
            var vB = _decelSpeed[cnt + 1];
            var vC = _decelSpeed[cnt + 2];
            var dv1 = vA - vB;
            var dv2 = vB - vC;
            var b = Math.Log(dv2 / dv1);
            var a = vB / Math.Exp(b * (cnt + 2));

            decelparams = new AccelParams { vStart = vA, a = a, b = b, vIdx = cnt };
            decelparams.SetupTableDecel(30.0, 0.1);
        }

        [DebuggerDisplay("{vStart}+n*{a}*exp({b}*n)")]
        public struct AccelParams
        {
            public int vIdx;
            public double vStart;
            public double a;
            public double b;
            public double GetSpeedStep(int n)
            {
                return vStart + (n-vIdx) * (a * Math.Exp(b * n));
            }

            public double GetSpeedDecel(int n)
            {
                return a * Math.Exp(b * n);
            }

            public override string ToString()
            {
                return string.Format("{0}+n*{1}*exp({2}*n)", vStart, a, b);
            }
            public double[] decel_tbl;
            internal void SetupTableDecel(double max, double min)
            {
                int low_idx = 1;
                var baseSpeed = GetSpeedDecel(low_idx);
                var curSpeed = baseSpeed;
                while (curSpeed < max)
                {
                    curSpeed = GetSpeedDecel(--low_idx);
                }
                int up_idx = 1;
                curSpeed = baseSpeed;
                while (curSpeed > min)
                {
                    curSpeed = GetSpeedDecel(++up_idx);
                }
                decel_tbl = new double[up_idx - low_idx];
                int cnt = 0;
                while (cnt < decel_tbl.Length)
                {
                    decel_tbl[cnt] = GetSpeedDecel(low_idx);
                    cnt++;
                    low_idx++;
                }
            }

            public int GetIndex(double wanted)
            {
                if (decel_tbl[0] < wanted)
                    return 0;
                if (decel_tbl[decel_tbl.Length - 1] > wanted)
                    return decel_tbl.Length - 1;
                int cnt = 0;
                while (decel_tbl[cnt] > wanted)
                    cnt++;
                return cnt;
            }
        }

        public int GetTicksToDecelerate(double current, double wanted)
        {
            if (current <= wanted)
                return 0;
            var idx_high = decelparams.GetIndex(current);
            var idx_low = decelparams.GetIndex(wanted);
            return idx_low - idx_high;            
        }

        internal int GetTicksToNextSpeedLimit(CarPosition myPos, double currentSpeed, out double nextSpeedLimit)
        {
            var pieceIdx = GetNextPieceWithDifferentSpeedLimit(myPos);
            nextSpeedLimit = currentSpeed;

            if (pieceIdx < 0)
                return -1;

            nextSpeedLimit = _pieceData[pieceIdx].maxSpeed[myPos.piecePosition.lane.endLaneIndex];

            var ppB = new PiecePosition { pieceIndex = pieceIdx, inPieceDistance = 0, lane = myPos.piecePosition.lane };
            var distance = GetDistance(myPos.piecePosition, ppB, myPos.piecePosition.lane.endLaneIndex);

            return (int)Math.Ceiling(distance / currentSpeed);
        }

        private int GetNextPieceWithDifferentSpeedLimit(CarPosition myPos)
        {
            var pieceId = myPos.piecePosition.pieceIndex;
            var lane = myPos.piecePosition.lane.endLaneIndex;
            var curlimit = _pieceData[pieceId].maxSpeed[lane];
            while (true)
            {
                pieceId = NormalizePid(++pieceId);
                if (_pieceData[pieceId].maxSpeed[lane] != curlimit)
                    return pieceId;
                // wrapped, not found?
                if (pieceId == myPos.piecePosition.pieceIndex)
                    return -1;
            }            
        }

        internal PiecePathData GetPiece(PiecePosition piecePosition)
        {
            if (piecePosition == null)
                return _pieceData[0];
            var pid = NormalizePid(piecePosition.pieceIndex);
            return _pieceData[pid];
        }
    }
}
